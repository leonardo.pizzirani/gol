import sys.process._

class Gol(newHeight: Int, newWidth: Int){
  val height: Int = newHeight
  val width: Int = newWidth
  var board: Array[Array[Int]] = Array.ofDim[Int](height, width)
  def fill(): Unit = {
    val random = scala.util.Random
    for(i <- 0 to height-1){
      for(j <- 0 to width-1){
        board(i)(j) = random.nextFloat().round
      }
    }
  }
  def show(): Unit = {
    for(i <- 0 to height-1){
      var line: String = ""
      for(j <- 0 to width-1){
        var char: String = "⬛"
        if(board(i)(j)==0){
          char = "⬜"
        }
        line = line + char
      }
      println(line)
    }
  }
  def neighs(i: Int, j: Int): Int = {
    val ii: Int = if(i-1<0) height-1 else i-1
    val jj: Int = if(j-1<0) width-1 else j-1
    return {
      board(ii)(jj) +
      board(ii)(j) +
      board(ii)((j+1)%width) +
      board(i)((j+1)%width) +
      board((i+1)%height)((j+1)%width) +
      board((i+1)%height)(j) +
      board((i+1)%height)(jj) +
      board(i)(jj)
    }
  }
  def step(n: Int = 1): Unit = {
    for(k <- 0 to n-1){
      var tempBoard: Array[Array[Int]] = Array.ofDim[Int](height, width)
      for(i <- 0 to height-1){
        for(j <- 0 to width-1){
          var nNeighs = neighs(i, j)
          if(board(i)(j) == 0){
            if(nNeighs == 3){
              tempBoard(i)(j) = 1
            }
          }else{
            if(nNeighs == 2 || nNeighs == 3){
              tempBoard(i)(j) = 1
            }
          }
        }
      }
      board = tempBoard
    }
  }
}


object Gol extends App{
  val FPS: Int = 10
  val height: Int = 50
  val width: Int = 50
  val g = new Gol(height, width)
  def update(dt: Long): Unit = {
    g.step()
  }
  def show(): Unit = {
    "clear".!
    g.show()
  }

  var lastFrameTime: Long = 0
  var currentTime: Long = 0
  var dt: Long = 0
  var sleepTime: Long = 0
  g.fill()
  while(true){
    currentTime = System.currentTimeMillis()
    dt = currentTime - lastFrameTime
    lastFrameTime = currentTime
    sleepTime = (1000.0/FPS).toLong - dt
    if(sleepTime > 0){
      Thread.sleep(sleepTime)
    }
    show()
    update(dt)
  }
}
