#include<math.h>
#include<time.h>

typedef struct Gol_struct Gol;

struct Gol_struct{
  int width;
  int height;
  int *board;
  void (*show)(const Gol *);
  void (*fill)(const Gol *);
  int (*neighs)(const Gol *, int, int);
  void (*step)(const Gol *, int);
};

void showGol(const Gol *self){
  for(int i=0; i<self->height; ++i){
    for(int j=0; j<self->width; ++j){
      if(self->board[i*self->height + j] == 1){
	printf("⬛");
      }else{
	printf("⬜");
      }
    }
    printf("\n");
  }
}
void fillGol(const Gol *self){
  for(int i=0; i<self->height; ++i){
    for(int j=0; j<self->width; ++j){
      self->board[i*self->height + j] = (int)round((float)rand()/RAND_MAX);
    }
  }
}
int neighsGol(const Gol *self, int i, int j){
  int ii, jj;
  if(i-1<0){
    ii = self->height-1;
  }else{
    ii = i-1;
  }
  if(j-1<0){
    jj = self->width-1;
  }else{
    jj = j-1;
  }
  return
    self->board[ii*self->height + jj] +
    self->board[ii*self->height + j] +
    self->board[ii*self->height + (j+1)%self->width] +
    self->board[i*self->height + (j+1)%self->width] +
    self->board[((i+1)%self->height)*self->height + (j+1)%self->width] +
    self->board[((i+1)%self->height)*self->height + j] +
    self->board[((i+1)%self->height)*self->height + jj] +
    self->board[i*self->height + jj];
}
void stepGol(const Gol *self, int n){
  int *tempBoard;
  int nNeighs;
  for(int k=0; k<n; ++k){
    tempBoard = (int *)calloc(self->height * self->width, sizeof(int));
    for(int i=0; i<self->height; ++i){
      for(int j=0; j<self->width; ++j){
	nNeighs = self->neighs(self, i, j);
	if(self->board[i*self->height + j] == 0){
	  if(nNeighs == 3){
	    tempBoard[i*self->height + j] = 1;
	  }
	}else{
	  if(nNeighs == 2 || nNeighs == 3){
	    tempBoard[i*self->height + j] = 1;
	  }
	}
      }
    }
    for(int i=0; i<self->height; ++i){
      for(int j=0; j<self->width; ++j){
	self->board[i*self->height + j] = tempBoard[i*self->height + j];
      }
    }
  }
  free(tempBoard);
}
Gol newGol(Gol *self, int height, int width){
  self->height = height;
  self->width = width;
  self->board = (int *)calloc(height * width, sizeof(int));
  self->show = &showGol;
  srand(time(NULL));
  self->fill = &fillGol;
  self->neighs = &neighsGol;
  self->step = &stepGol;
  return *self;
}
