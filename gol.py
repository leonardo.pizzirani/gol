#!/usr/bin/env python
import random
import time
import os

class Gol:
    width = 0
    height = 0
    board = []
    def __init__(self, height=50, width=50):
        self.width = width
        self.height = height
        self.board = [ [ 0 for _ in range(self.width) ] for _ in range(self.height) ] 
        self.fill()
        self.show()
    def fill(self):
        for i in range(self.height):
            for j in range(self.width):
                self.board[i][j] = round(random.random())
    def show(self):
        for i in range(self.height):
            line = ""
            for j in range(self.width):
                char = "⬛"
                if self.board[i][j] == 0:
                    char = "⬜"
                line = line + char
            print(line)
    def neighs(self, i, j):
        return self.board[(i-1)%self.height][(j-1)%self.width] + \
               self.board[(i-1)%self.height][j] + \
               self.board[(i-1)%self.height][(j+1)%self.width] + \
               self.board[i][(j+1)%self.width] + \
               self.board[(i+1)%self.height][(j+1)%self.width] + \
               self.board[(i+1)%self.height][j] + \
               self.board[(i+1)%self.height][(j-1)%self.width] + \
               self.board[i][(j-1)%self.width]
    def step(self, n=1):
        for k in range(n):
            temp_board = [ [ 0 for _ in range(self.width) ] for _ in range(self.height) ] 
            for i in range(self.height):
                for j in range(self.width):
                    n_neighs = self.neighs(i, j)
                    if self.board[i][j] == 0:
                        if n_neighs == 3:
                            temp_board[i][j] = 1
                    else:
                        if n_neighs == 2 or n_neighs == 3:
                            temp_board[i][j] = 1
            self.board = temp_board

if __name__ == "__main__":
    FPS = 10
    height = 50
    width = 50
    g = Gol(height, width)
    def update(dt):
        g.step()
    def show():
        os.system("clear")
        g.show()
        
    last_frame_time = 0.0
    while True:
        current_time = time.time()
        dt = current_time - last_frame_time
        last_frame_time = current_time
        sleep_time = 1.0/FPS - dt
        if sleep_time > 0.0:
            time.sleep(sleep_time)
        show()
        update(dt)
