# coding: utf-8
class Gol
  @height = 0
  @width = 0
  def initialize(height, width)
    @height = height
    @width = width
    @board = Array.new(@height) { Array.new(@width) { 0 } }
  end
  def show()
    for i in 0..@height-1
      for j in 0..@width-1
        if @board[i][j] == 1
          print "⬛"
        else
          print "⬜"
        end
      end
      print "\n"
    end
  end
  def fill()
    for i in 0..@height-1
      for j in 0..@width-1
        @board[i][j] = [1, 0].sample
      end
    end
  end
  def neighs(i, j)
    return @board[(i-1)%@height][(j-1)%@width] + \
           @board[(i-1)%@height][j] + \
           @board[(i-1)%@height][(j+1)%@width] + \
           @board[i][(j+1)%@width] + \
           @board[(i+1)%@height][(j+1)%@width] + \
           @board[(i+1)%@height][j] + \
           @board[(i+1)%@height][(j-1)%@width] + \
           @board[i][(j-1)%@width]
  end
  def step(n=1)
    for k in 0..n-1
      tempBoard = Array.new(@height) { Array.new(@width) { 0 } }
      for i in 0..@height-1
        for j in 0..@width-1
          nNeighs = neighs(i, j)
          if @board[i][j] == 1
            if nNeighs == 2 or nNeighs == 3
              tempBoard[i][j] = 1
            end
          else
            if nNeighs == 3
              tempBoard[i][j] = 1
            end
          end
        end
      end
      @board = tempBoard
    end
  end
end

def update(dt)
  $g.step
end
def show
  system("clear")
  $g.show
end

def main
  height = 50
  width = 50
  $g = Gol.new(height, width)
  $g.fill
  fps = 10
  lastFrameTime = 0.0
  while true
    currentTime = Float(Time.now)
    dt = currentTime - lastFrameTime
    lastFrameTime = currentTime
    sleepTime = 1.0/fps - dt
    if sleepTime > 0.0
      sleep(sleepTime)
    end
    show
    update(dt)
  end
end

main
