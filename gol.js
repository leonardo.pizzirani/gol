class Gol{
    constructor(height, width){
	this.height = height;
	this.width = width;
	this.board = new Array(this.height);
	for (var i=0; i<this.board.length; i++){
	    this.board[i] = new Array(this.width);
	}
	for(var i=0; i<this.height; i++){
	    for(var j=0; j<this.width; j++){
		this.board[i][j] = 0
	    }
	}
    }
    fill(){
	for(var i=0; i<this.height; i++){
	    for(var j=0; j<this.width; j++){
		this.board[i][j] = Math.round(Math.random());
	    }
	}
    }
    show(){
	for(var i=0; i<this.height; i++){
	    var line = "";
	    for(var j=0; j<this.width; j++){
		var char = "⬛";
		if(this.board[i][j] == 0){
		    char = "⬜";
		}
		line = line + char;
	    }
	    console.log(line);
	}
    }
    neighs(i, j){
	var ii = i-1<0 ? this.height-1 : i-1;
	var jj = j-1<0 ? this.width-1 : j-1;
	return this.board[ii][jj] +
	    this.board[ii][j] +
	    this.board[ii][(j+1)%this.width] +
	    this.board[i][(j+1)%this.width] +
	    this.board[(i+1)%this.height][(j+1)%this.width] +
	    this.board[(i+1)%this.height][j] +
	    this.board[(i+1)%this.height][jj] +
	    this.board[i][jj];
    }
    step(n = 1){
	for(var k=0; k<n; ++k){
	    var tempBoard = new Array(this.height);
	    for (var i=0; i<tempBoard.length; i++){
		tempBoard[i] = new Array(this.width);
	    }
	    for(var i=0; i<this.height; i++){
		for(var j=0; j<this.width; j++){
		    tempBoard[i][j] = 0;
		}
	    }
	    for(var i=0; i<this.height; i++){
		for(var j=0; j<this.width; j++){
		    var nNeighs = this.neighs(i, j);
		     if(this.board[i][j] == 0){
			 if(nNeighs == 3){
			     tempBoard[i][j] = 1;
			 }
		     }else{
			 if(nNeighs == 2 || nNeighs == 3){
			     tempBoard[i][j] = 1;
			 }
		     }
		}
	    }
	    this.board = tempBoard;
	}
    }
}

var FPS = 10;
var height = 50;
var width = 50;
var g = new Gol(height, width);
function update(dt){
    g.step();
}
function show(){
    console.clear();
    g.show();
}
function loop(){
}
async function main(){
    g.fill();
    g.show();
    var lastFrameTime = 0.0;
    date = new Date();
    while(true){
	var currentTime = date.getTime();
	var dt = currentTime - lastFrameTime;
	lastFrameTime = currentTime;
	var sleepTime = 1000.0/FPS - dt;
	if(sleepTime > 0){
	    await new Promise(r => setTimeout(r, sleepTime)); 
	}
	show();
	update(dt);
    }
}
main();
