class Gol{
    constructor(height, width, side){
        this.height = height;
        this.width = width;
        this.side = side;
        this.board = new Array(this.height);
        for (var i=0; i<this.board.length; i++){
            this.board[i] = new Array(this.width);
        }
        for(i=0; i<this.height; i++){
            for(var j=0; j<this.height; j++){
                this.board[i][j] = 0;
            }
        }
    }
    fill(){
        for(var i=0; i<this.height; i++){
            for(var j=0; j<this.width; j++){
                this.board[i][j] = Math.round(Math.random());
            }
	    }
	}
    show(){
        for(var i=0; i<this.height; i++){
            for(var j=0; j<this.width; j++){
                strokeWeight(0);
                if (this.board[i][j]==1){
                    fill(0);
                }else{
                    fill(255);
                }
                square(i*this.side, j*this.side, this.side);
            }
	    }
    }
    neighs(i, j){
        var ii = i-1<0 ? this.height-1 : i-1;
        var jj = j-1<0 ? this.width-1 : j-1;
        return this.board[ii][jj] +
            this.board[ii][j] +
            this.board[ii][(j+1)%this.width] +
            this.board[i][(j+1)%this.width] +
            this.board[(i+1)%this.height][(j+1)%this.width] +
            this.board[(i+1)%this.height][j] +
            this.board[(i+1)%this.height][jj] +
            this.board[i][jj];
    }
    step(n = 1){
        for(var k=0; k<n; ++k){
            var tempBoard = new Array(this.height);
            for (var i=0; i<tempBoard.length; i++){
                tempBoard[i] = new Array(this.width);
            }
            var j;
            for(i=0; i<this.height; i++){
                for(j=0; j<this.width; j++){
                    tempBoard[i][j] = 0;
                }
            }
            for(i=0; i<this.height; i++){
                for(j=0; j<this.width; j++){
                    var nNeighs = this.neighs(i, j);
                    if (this.board[i][j] == 0) {
                        if (nNeighs == 3) {
                            tempBoard[i][j] = 1;
                        }
                    } else {
                        if (nNeighs == 2 || nNeighs == 3) {
                            tempBoard[i][j] = 1;
                        }
                    }
                }
            }
            this.board = tempBoard;
        }
    }
}

var height = 40;
var width = 80;
var side = 10;
var gol = new Gol(width, height, side);
gol.fill();
function setup(){
    createCanvas(width*side, height*side);
}
function draw(){
    frameRate(10);
    gol.step();
    gol.show();
}