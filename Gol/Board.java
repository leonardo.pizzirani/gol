package Gol;
import java.lang.Math;
public class Board {
    private int height;
    private int width;
    private int[][] board;

    public Board(int height, int width){
	this.height = height;
	this.width = width;
	this.board = new int[this.height][this.width];
	this.fillBoard();
    }

    private final void fillBoard() {
	for (int i = 0; i < this.height; i++) {
	    for (int j = 0; j < this.width; j++) {
		this.board[i][j] = (int) Math.round(Math.random());
	    }
	}
    }

    private final int neighs(int i, int j) {
	int im = (i - 1 < 0) ? (this.height - 1) : (i - 1);
	int jm = (j - 1 < 0) ? (this.width - 1) : (j - 1);
	int ip = (i + 1)%this.height;
	int jp = (j + 1)%this.width;
	
	return
	    this.board[im][jm] +
	    this.board[im][j ] +
	    this.board[im][jp] +
	    this.board[i ][jp] +
	    this.board[ip][jp] +
	    this.board[ip][j ] +
	    this.board[ip][jm] +
	    this.board[i ][jm];
    }

    public final void step() {
	int[][] tmpBoard = new int[this.height][this.width];
	int neighs;
	for (int i = 0; i < this.height; i++) {
	    for (int j = 0; j < this.width; j++) {
		neighs = this.neighs(i, j);
		if (this.board[i][j] == 0) {
		    if (neighs == 3) {
			tmpBoard[i][j] = 1;
		    }
		} else {
		    if ((neighs == 2) || (neighs == 3)) {
			tmpBoard[i][j] = 1;
		    }
		}
	    }
	}
	this.board = tmpBoard;
    }
    
    public final void printBoard() {
	for (int i = 0; i < this.height; i++) {
	    for (int j = 0; j < this.width; j++) {
		if (this.board[i][j] == 0) {
		    System.out.print("⬜");
		} else {
		    System.out.print("⬛");
		}
	    }
	    System.out.println();
	}
    }
}
