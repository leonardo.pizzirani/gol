package Gol;
import java.util.Date;
class Main {
    public static void main(String[] args) throws InterruptedException {
	final double msPF = 1000.0/6.0;
	final int height = 30;
	final int width = 40;
	Board board = new Board(height, width);
	Date date = new Date();
	double t0 = 0.0;
	double t1;
	double dt;
	double sleep;
	while (true) {
	    t1 = date.getTime();
	    dt = t1 - t0;
	    t0 = t1;
	    sleep = msPF - dt;
	    if (sleep > 0.0) {
		Thread.sleep((long) sleep);
	    }
	    System.out.println("\033[H\033[2J");
	    board.printBoard();
	    board.step();
	}
    }
}
