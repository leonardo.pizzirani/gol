#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<unistd.h>

#include"gol.h"

#define FPS 10.0
#define HEIGHT 50
#define WIDTH 50

void main(){
  Gol g = newGol(&g, HEIGHT, WIDTH);
  double lastFrameTime = 0.0;
  double currentTime = 0.0;
  double dt = 0.0;
  double sleepTime = 0.0;
  
  g.fill(&g);
  for(;;){
    currentTime = time(NULL);
    dt = currentTime - lastFrameTime;
    lastFrameTime = currentTime;
    sleepTime = 1000.0/FPS - dt;
    if(sleepTime > 0.0){
      usleep(1000*sleepTime);
    }
    printf("\e[1;1H\e[2J");
    fflush(stdout);
    g.show(&g);
    sleep(0.5);
    g.step(&g, 1);
  }
}
